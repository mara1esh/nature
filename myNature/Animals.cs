﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myNature
{
	class Animals
	{
		public string Title { get; set; }
		public string Kind { get; set; }
		public string Type { get; set; }
		public string Family { get; set; }
		public string Population { get; set; }
		public int Age { get; set; }
		public double Weight { get; set; }
		public int Growth { get; set; }
		public void Move ()
		{
			Console.WriteLine("I make one step");
		}
		public double IndexDodyWeight()
		{
			return (Weight / (Growth * Growth));
		}

        public int valeriiMethod()
        {
            Console.WriteLine("the operation was successful!");
            return -1;
        }
	}
}
