﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myNature
{
    class Vegetables
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }

        public Vegetables(int ID, string Name, string Color)
        {
            this.ID = ID;
            this.Name = Name;
            this.Color = Color;
        }
    }
}
