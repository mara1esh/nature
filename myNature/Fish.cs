﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myNature
{
    class Fish
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
		public string TypeFish { get; set; }
		public void ChangeColor(string color)
		{
			Color = color;
		}
		public void ChangeName(string Name)
		{
			this.Name = Name;
		}
    }
}
